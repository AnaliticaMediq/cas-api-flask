import pandas as pd
import numpy as np

import pymssql
import pandas.io.sql as psql

from sqlalchemy import create_engine
from sqlalchemy.engine.url import URL

import json


output_path = './data/input_notes/'


# obtener credenciales
with open('etl-credentials.json') as json_file:
    json_credentials = json.load(json_file)


database = json_credentials['database']
username =  json_credentials['username']
password = json_credentials['password']
host = json_credentials['host']

# sqlalchemy engine
ddbb_credentials = URL(
    drivername="mssql+pymssql",
    username=username,
    password=password,
    host=host,
    database=database
)

query_dx = '''
        SELECT *
        FROM ''' + database + '''.dbo.NotasDX;
        '''

query_px = '''
        SELECT *
        FROM ''' + database + '''.dbo.NotasPX;
        '''


def extract_data(query, ddbb_credentials, output_path, filename):
    # Asegurar correcto formato de query
    def debugLogSQL(query):
         return ' '.join([line.strip() for line in query.splitlines()]).strip()


    chunksize = 100000
    engine = create_engine(ddbb_credentials)

    df = pd.DataFrame()

    with engine.connect() as conn:

        clean_query = debugLogSQL(query)
        generator_df = pd.read_sql(sql=clean_query,  # mysql query
                                   con=conn,
                                   chunksize=chunksize)  # size you want to fetch each time

        for dataframe in generator_df:
            df = df.append(dataframe)

        conn.close()

    df.to_csv(output_path+filename, header=True, index=False)

extract_data(query_dx, ddbb_credentials, output_path, 'NotasDX.csv')
extract_data(query_px, ddbb_credentials, output_path, 'NotasPX.csv')


import numpy as np
import pandas as pd


def get_top_n_match(row, n_top=5):
    """
    :param row:
    :param n_top: number of results to be determined
    :return: list of tuples with index of the match and the cosine similarity score
    """

    row_count = row.getnnz()
    if row_count == 0:
        return None
    elif row_count <= n_top:
        result = zip(row.indices, row.data)
    else:
        arg_idx = np.argpartition(row.data, -n_top)[-n_top:]
        result = zip(row.indices[arg_idx], row.data[arg_idx])
    return sorted(result, key=(lambda x: -x[1]))


def match(input_name, vectorizer, text_vectors, corpus, tipo, df_dx, df_px):
    """
    :param input_name: input text whose matches need to be found
    :param vectorizer: TFIDF vectorizer which was initialized earlier
    :param comp_name_vectors: the company names' vectors of the whole data set
    :param comp_name_df: the company names dataframe
    :return: a dataframe with top N matching names with match score
    """

    input_name_vector = vectorizer.transform([input_name])
    result_vector = input_name_vector.dot(text_vectors.T)
    matched_data = [get_top_n_match(row) for row in result_vector]
    # print(matched_data)
    if not None in matched_data:
        flat_matched_data = [tup for data_row in matched_data for tup in data_row]
        # print(flat_matched_data)
        lkp_idx, lkp_sim = zip(*flat_matched_data)
        nr_matches = len(lkp_idx)
        matched_ids = np.empty([nr_matches], dtype=object)
        matched_names = np.empty([nr_matches], dtype=object)
        matched_users = np.empty([nr_matches], dtype=object)
        matched_codes = np.empty([nr_matches], dtype=object)
        # print(matched_names)
        sim = np.zeros(nr_matches)
        for i in range(nr_matches):
            matched_ids[i] = lkp_idx[i]
            matched_names[i] = corpus[lkp_idx[i]]
            if tipo == '1':
                matched_users[i] = df_dx['Usuario'][lkp_idx[i]]
                matched_codes[i] = df_dx['CodigoDX'][lkp_idx[i]]
            if tipo == '2':
                matched_users[i] = df_px['Usuario'][lkp_idx[i]]
                matched_codes[i] = df_px['CodigoPX'][lkp_idx[i]]
            sim[i] = lkp_sim[i]
        return pd.DataFrame({"Nota": input_name,
                             "Id match": matched_ids,
                             "Match": matched_names,
                             "User": matched_users,
                             "Code": matched_codes,
                             "Score": sim * 100})
    return None

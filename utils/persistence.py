from csv import writer
import datetime

def create_csv(file_name):
    list_of_elem = ["LogSugerenciaID", "SugerenciaNotaDiagID", "Nota", "Sugerido", "UserName", "Confianza"]
    # Open file in append mode
    with open(file_name, 'w', newline='') as write_obj:
        # Create a writer object from csv module
        csv_writer = writer(write_obj)
        # Add contents of list as last row in the csv file
        csv_writer.writerow(list_of_elem)
        write_obj.flush()


# with json_response = {"LogSugerenciaID": 0, "Sugerencias": lista_respuestas}
def save_response(file_name, json_response):
    # Open file in append mode
    with open(file_name, 'a+', newline='') as write_obj:
        # Create a writer object from csv module
        csv_writer = writer(write_obj)
        # Add contents of list as last row in the csv file
        for sugerencia in json_response['Sugerencias']:
            new_row = [json_response['LogSugerenciaID'], json_response['CodHospital'], json_response['CodEpisodio'], *list(sugerencia.values()), str(datetime.datetime.now())]
            csv_writer.writerow(new_row)
        write_obj.flush()


def get_last_id(file_name):
    with open(file_name, "r") as read_obj:
        last_line = read_obj.readlines()[-1]
        last_id = int(last_line.split(',')[0])
    return last_id
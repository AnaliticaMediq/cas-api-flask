#!flask/bin/python
import pandas as pd

from flask import Flask, jsonify, make_response, request

import threading

from utils.persistence import save_response, get_last_id
from utils.match import get_top_n_match, match

from sklearn.feature_extraction.text import TfidfVectorizer


app = Flask(__name__)

# example message
# http://127.0.0.1:3001/mediq/cas/?tipo=1&codHospital=123&codEpisodio=123&userName=carolina.torres@mediq.cl&nota=absceso


# ToDo Agregar frecuencia de nota->codigo para entregar preferencia a patrones más comunes
# ToDo crear función para descargar data
# ToDo agregar seguridad
# ToDo agregar errores si esque no se envia usuario correcto, tipo correcto, etc (coordinar con TI)
# ToDo arreglar paths


# Dirs and paths
data_dx_path = './data/input_notes/NotasDX.csv'
data_px_path = './data/input_notes/NotasPX.csv'

responses_persistence_path = 'data/responses_persistence/persistence.csv'

df_dx = pd.read_csv(data_dx_path, dtype=str).fillna('###').astype(str)  # se detectaron algunos nan en la tabla de notas de proc
df_px = pd.read_csv(data_px_path, dtype=str).fillna('###').astype(str)

corpus_dx = df_dx['NotaDX']
corpus_px = df_px['NotaPX']

tfidf = TfidfVectorizer(min_df=1, strip_accents='unicode', lowercase=True)

current_id = get_last_id(responses_persistence_path) + 1

@app.route('/mediq/cas/', methods=['GET'])
def cas():
    global current_id
    tipo = request.args.get('tipo', '1')
    cod_hospital = request.args.get('codHospital', None)
    cod_episodio = request.args.get('codEpisodio', None)
    username = request.args.get('userName', None)
    nota_ingresada = request.args.get('nota', None)

    # el proceso es para diagnosticos o procedimientos
    if tipo == '1':
        text_vectors = tfidf.fit_transform(corpus_dx)
        corpus = corpus_dx

    if tipo == '2':
        text_vectors = tfidf.fit_transform(corpus_px)
        corpus = corpus_px

    # find matches
    df_matches = match(nota_ingresada, tfidf, text_vectors, corpus, tipo, df_dx, df_px)

    # se instancia respuesta json, la cual tiene un contrato con la funcion de persistencia: save_response
    # response {"LogSugerenciaID": 0, "CodHospital": 123456, "CodEpisodio": 123456,  "Sugerencias": lista_respuestas}
    response = {"LogSugerenciaID": current_id, "CodHospital": cod_hospital, "CodEpisodio": cod_episodio}


    # si existen matches en el df se agregan a la respuesta json
    if df_matches is not None:
        sugestion_list = []  # lista de sugerencias con toda su info
        # se agrega lógica de dar prioridad al mismo usuario y eliminar duplicados
        df_matches['is_from_user'] = df_matches['User'] == username
        df_matches = df_matches.sort_values(by='is_from_user', ascending=False)
        df_matches = df_matches.drop_duplicates(['Code'], keep='first').reset_index(drop=True)

        i = 0
        for i in range(df_matches.shape[0]):
            sugestion = {}
            sugestion['SugerenciaNotaDiagID'] = int(df_matches['Id match'][i])
            sugestion['Nota'] = df_matches['Nota'][i]
            sugestion['Sugerido'] = df_matches['Code'][i]
            sugestion['UserName'] = df_matches['User'][i]
            sugestion['Confianza'] = round(df_matches['Score'][i], 2)
            sugestion_list.append(sugestion)
            i = i + 1

    else:
        sugestion_list = None
    response['Sugerencias'] = sugestion_list

    # si existen matches, persistencia de respuesta en un hilo paralelo
    if response['Sugerencias'] is not None:
        current_id = current_id + 1
        # crear hilo que almacene la respuesa en un .txt
        thread = threading.Thread(target=save_response, kwargs={'file_name': './data/responses_persistence/persistence.csv', 'json_response': response})
        thread.start()


    return jsonify({'statusCode': 200, 'body': response})


@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)


if __name__ == '__main__':
    app.run(debug=False, host='0.0.0.0', port=3001)
